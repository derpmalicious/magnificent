extern crate magnificent;

use magnificent::model_o_minus;
use clap::{Arg, App};
use std::io::{Error, ErrorKind};

fn hex2color(c: &str) -> Result<model_o_minus::Color, Box<dyn std::error::Error>> {
	if c.len() != 6 {
		return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid color")))
	}
	
	let r = u8::from_str_radix(&c[0..2], 16)?;
	let g = u8::from_str_radix(&c[2..4], 16)?;
	let b = u8::from_str_radix(&c[4..6], 16)?;
	Ok(model_o_minus::Color::new(r, g, b))
}

fn main() {
	if let Err(e) = main_wrapped() {
		eprintln!("Error: {}", e.description());
	}
}

fn main_wrapped() -> Result<(), Box<dyn std::error::Error>> {
	let matches = App::new("Magnificent")
		.version("0.1.0")
		.author("malice <derpmalicious@gmail.com>")
		.about("Glorious Model O- configuration utility")
		.arg(Arg::with_name("animation")
			.short("a")
			.long("animation")
			.value_name("ANIMATION")
			.help("Sets the animation")
			.possible_values(
				&["glorious", "single", "tail", "off", "breathing_rgb", "breathing_single", "rave", "wave",
				"breathing", "6"])
			.default_value("glorious")
			.takes_value(true))
		.arg(Arg::with_name("speed")
			.short("s")
			.long("speed")
			.value_name("SPEED")
			.help("Sets the animation speed")
			.possible_values(&["1", "2", "3"])
			.default_value("2")
			.takes_value(true))
		.arg(Arg::with_name("brightness")
			.short("b")
			.long("brightness")
			.value_name("BRIGHTNESS")
			.help("Sets the animation brightness")
			.possible_values(&["25", "50", "75", "100"])
			.default_value("100")
			.takes_value(true))
		.arg(Arg::with_name("direction")
			.short("j")
			.long("direction")
			.value_name("DIRECTION")
			.help("Sets the animation direction")
			.possible_values(&["up", "down"])
			.default_value("down")
			.takes_value(true))
		.arg(Arg::with_name("poll")
			.short("p")
			.long("poll")
			.value_name("POLLING_RATE")
			.help("Sets the polling rate")
			.possible_values(&["125", "250", "500", "1000"])
			.default_value("1000")
			.takes_value(true))
		.arg(Arg::with_name("lod")
			.short("l")
			.long("lod")
			.value_name("LOD")
			.help("Sets the LOD")
			.possible_values(&["2", "3"])
			.default_value("2")
			.takes_value(true))
		.arg(Arg::with_name("debounce")
			.short("e")
			.long("debounce")
			.value_name("DEBOUNCE")
			.help("Sets the debounce time")
			.default_value("4")
			.takes_value(true))
		.arg(Arg::with_name("colors")
			.short("c")
			.long("colors")
			.value_name("COLORS")
			.help("Sets the colors used in animations (Space separated list)")
			// TODO: Can't set default color list to contain multiple colors, https://github.com/clap-rs/clap/issues/1452
			.default_value("ff0000")
			.min_values(1)
			.max_values(7)
			.takes_value(true))
		.arg(Arg::with_name("profiles")
			.short("d")
			.long("profiles")
			.value_name("PROFILES")
			.help("Sets the dpi profiles (Space separated list)")
			// TODO: Can't set default list to contain multiple profiles, https://github.com/clap-rs/clap/issues/1452
			.default_value("1600:ff0000")
			.min_values(1)
			.max_values(8)
			.takes_value(true))
		.get_matches();
	
	
	let animation = matches.value_of("animation").unwrap();
	let speed = matches.value_of("speed").unwrap();
	let brightness = matches.value_of("brightness").unwrap();
	let direction = matches.value_of("direction").unwrap();
	let poll = matches.value_of("poll").unwrap();
	let lod = matches.value_of("lod").unwrap();
	let debounce = matches.value_of("debounce").unwrap();
	let colors = matches.values_of("colors").unwrap();
	let profiles = matches.values_of("profiles").unwrap();
	
	// TODO: Validation, warning/error if an argument is passed that the animation doesn't support
	// TODO: Move some of these to traits or functions or something
	
	let speed = match speed {
		"1" => model_o_minus::Speed::Speed1,
		"2" => model_o_minus::Speed::Speed2,
		"3" => model_o_minus::Speed::Speed3,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid speed")))
	};
	
	let direction = match direction {
		"up" => model_o_minus::UpDownDirection::Up,
		"down" => model_o_minus::UpDownDirection::Down,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid direction")))
	};
	
	let brightness = match brightness {
		"25" => model_o_minus::Brightness::B25,
		"50" => model_o_minus::Brightness::B50,
		"75" => model_o_minus::Brightness::B75,
		"100" => model_o_minus::Brightness::B100,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid brightness")))
	};
	
	let poll = match poll {
		"125" => model_o_minus::PollingRate::Poll125,
		"250" => model_o_minus::PollingRate::Poll250,
		"500" => model_o_minus::PollingRate::Poll500,
		"1000" => model_o_minus::PollingRate::Poll1000,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid polling rate")))
	};
	
	let lod = match lod {
		"2" => model_o_minus::LOD::LOD2mm,
		"3" => model_o_minus::LOD::LOD3mm,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid lod")))
	};
	
	let colors: Vec<model_o_minus::Color> = colors.filter_map(|c| {
		let h = hex2color(c);
		if h.is_err() {
			eprintln!("Invalid color: {}, skipping", c);
		}
		h.ok()
	}).collect();
	
	let mut model_o_minus = model_o_minus::ModelOMinus::new()?;
	
	match animation {
		"glorious" => model_o_minus.set_animation_glorious(speed, direction),
		"single" => {
			if colors.is_empty() {
				return Err(Box::new(Error::new(ErrorKind::NotFound, "Not enough colors for the animation")))
			}
			model_o_minus.set_animation_single(brightness, colors[0])
		},
		"tail" => model_o_minus.set_animation_tail(brightness, speed),
		"off" => model_o_minus.set_animation_off(),
		"breathing_rgb" => model_o_minus.set_animation_breathing_rgb(speed),
		"breathing_single" => {
			if colors.is_empty() {
				return Err(Box::new(Error::new(ErrorKind::NotFound, "Not enough colors for the animation")))
			}
			model_o_minus.set_animation_single_color_breathing(speed, colors[0])
		},
		"rave" => {
			if colors.len() < 2 {
				return Err(Box::new(Error::new(ErrorKind::NotFound, "Not enough colors for the animation")))
			}
			model_o_minus.set_animation_rave(speed, brightness, colors[0], colors[1]);
		},
		"wave" => model_o_minus.set_animation_wave(brightness, speed),
		"breathing" => {
			if colors.len() < 2 {
				return Err(Box::new(Error::new(ErrorKind::NotFound, "Not enough colors for the animation")))
			}
			model_o_minus.set_animation_breathing(speed, &colors)?
		},
		"6" => model_o_minus.set_animation_6(&colors)?,
		_ => return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid animation")))
	}
	
	if profiles.len() < 1 {
		return Err(Box::new(Error::new(ErrorKind::NotFound, "Need at least one dpi profile")))
	}
	
	let mut i: u8 = 0;
	for profile in profiles {
		let profile: Vec<&str> = profile.split(':').collect();
		if profile.len() != 2 {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Dpi profiles have the format of dpi:color (For example: 1600:ff0000)")))
		}
		let dpi: i32 = profile[0].parse()?;
		let color = hex2color(profile[1])?;
		model_o_minus.set_dpi_profile(i as usize, dpi, color)?;
		
		i += 1;
	}
	
	// Ugly af, but not sure how to wrangle types in a way that makes ranges usable for this directly
	let mut enabledprofiles: Vec<u8> = Vec::new();
	for j in 0..i {
		enabledprofiles.push(j);
	}
	
	model_o_minus.set_enabled_dpi_profiles(&enabledprofiles, 0)?;
	
	model_o_minus.set_polling_rate(poll);
	//model_o_minus.set_lod(lod);
	model_o_minus.update_debounce(debounce.parse::<u8>()?)?;
	
	model_o_minus.update_device()?;
	Ok(())
}
