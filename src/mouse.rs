use rusb;
use std::io::Error;
use std::io::ErrorKind;

/// General mouse stuff
pub trait Mouse {
	/// USB vendor id
	fn vendor_id() -> u16;
	
	/// USB product id
	fn product_id() -> u16;
	
	/// Get control of the usb device, return a device handle
	fn usb_init() -> Result<rusb::DeviceHandle<rusb::GlobalContext>, Box<dyn std::error::Error>> {
		let mut device = match rusb::open_device_with_vid_pid(Self::vendor_id(), Self::product_id()) {
			Some(v) => v,
			None => return Err(Box::new(Error::new(ErrorKind::NotFound, "No device found")))
		};
		
		/* Claim stuff */
		for i in 0..2 {
			if device.kernel_driver_active(i)? {
				device.detach_kernel_driver(i)?;
			}
			
			device.claim_interface(i)?;
		}
		
		Ok(device)
	}
	
	/// Write a control message to the device
	fn write_control(&mut self, wvalue: u16, buf: &[u8]) -> Result<(), Box<dyn std::error::Error>>;
}
