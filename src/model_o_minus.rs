use rusb;
use crate::mouse::Mouse;
use std::time::Duration;
use std::vec::Vec;
use std::io::{Error, ErrorKind};

/// Enum that describes up/down direction
#[derive(Debug, Clone, Copy)]
pub enum UpDownDirection {
	Up = 1,
	Down = 0
}

/// Valid LOD values for Model O-
#[derive(Debug, Clone, Copy)]
pub enum LOD {
	LOD2mm = 1,
	LOD3mm = 2
}

/// Enum that contains possible polling rate values
#[derive(Debug, Clone, Copy)]
pub enum PollingRate {
	Poll1000 = 0x04,
	Poll500 = 0x03,
	Poll250 = 0x02,
	Poll125 = 0x01
}

/// Enum that contains possible speed values
#[derive(Debug, Clone, Copy)]
pub enum Speed {
	Speed1 = 1,
	Speed2 = 2,
	Speed3 = 3
}

/// Color
#[derive(Debug, Clone, Copy)]
pub struct Color {
	r: u8,
	g: u8,
	b: u8
}

/// Brightness
#[derive(Debug, Clone, Copy)]
pub enum Brightness {
	B25 = 0x10,
	B50 = 0x20,
	B75 = 0x30,
	B100 = 0x40
}

impl Color {
	/// Create a new color
	pub fn new(r: u8, g: u8, b: u8) -> Self{
		Color{ r, g, b }
	}
}

/// Struct representing a Model O- mouse
pub struct ModelOMinus {
	profiles_max: u8,
	data: Vec<u8>,
	device: rusb::DeviceHandle<rusb::GlobalContext>
}

impl Mouse for ModelOMinus {
	fn vendor_id() -> u16 { 0x258a }
	fn product_id() -> u16 { 0x0036 }
	
	fn write_control(&mut self, wvalue: u16, buf: &[u8]) -> Result<(), Box<dyn std::error::Error>> {
		/* Write data */
		self.device.write_control(0x21, 0x09, wvalue, 1, &buf, Duration::new(0, 0))?;
		Ok(())
	}
}

impl Drop for ModelOMinus {
	/// Free stuff. TODO: Would be nice if it came from the Mouse trait somehow
	fn drop(&mut self) {
		/* Unclaim */
		for i in 0..2 {
			self.device.release_interface(i).unwrap_or_else(|v| eprintln!("{:?}", v));
			self.device.attach_kernel_driver(i).unwrap_or_else(|v| eprintln!("{:?}", v));
		}
	}
}

impl ModelOMinus {
	/// Initialize and get a new Model O- Mouse struct thingy
	pub fn new() -> Result<Self, Box<dyn std::error::Error>> {
		let mut buf: Vec<u8> = vec![0; 520];
		
		/* TODO in future: get current values from mouse */
		Self::_set_defaults(&mut buf);
		
		Ok(ModelOMinus { data: buf, device: ModelOMinus::usb_init()?, profiles_max: 8 })
	}
	
	/// Set usable working defaults
	pub fn set_defaults(&mut self) {
		Self::_set_defaults(&mut self.data);
	}
	
	fn _set_defaults(buf: &mut Vec<u8>) {
		/* Set unknowns */
		buf[0] = 0x04;
		buf[1] = 0x11;
		buf[2] = 0x00;
		buf[3] = 0x7b;
		buf[4] = 0x00;
		buf[5] = 0x00;
		buf[6] = 0x00;
		buf[7] = 0x00;
		buf[8] = 0x64;
		buf[9] = 0x06;
		
		/* DPI stuff */
		buf[10] = 0x04; // dpi X = Y; 1000Hz; Technically, changing this will change the dpi, but I am not sure if it's the correct or safe way to do it, as both the dpi and debounce time are also sent in a second separate package too
		buf[11] = 0x11; // Dpi profiles enabled
		buf[12] = 0xfb; // Dpi profiles enabled
		
		/* I haven't bothered figuring out the dpi profile enable thing yet, so, set all of them to same for now */
		buf[13] = 0x0f; // Dpi profile 1 X = Y dpi
		buf[14] = 0x0f; // Dpi profile 2 X = Y dpi
		buf[15] = 0x0f; // Dpi profile 3 X = Y dpi
		buf[16] = 0x0f; // Dpi profile 4 X = Y dpi
		buf[17] = 0x0f; // Dpi profile 5 X = Y dpi
		buf[18] = 0x0f; // Dpi profile 6 X = Y dpi
		
		/* Animation stuff */
		buf[53] = 0x01; // Animation (1 = glorious)
		buf[54] = 0x43; // Glorious speed
		buf[55] = 0x00; // Animation direction ( 1 = up, 0 = down );
		
		/* Set unknowns.
		 * I THINK these are for some unused animation, but I am not 100% sure.
		 * Setting just in case, to avoid bricking mouse */
		buf[85] = 0x00;
		buf[86] = 0xff;
		buf[87] = 0x00;
		buf[88] = 0x00;
		buf[89] = 0x00;
		buf[90] = 0xff;
		buf[91] = 0x00;
		buf[92] = 0x00;
		buf[93] = 0x00;
		buf[94] = 0xff;
		buf[95] = 0xff;
		buf[96] = 0xff;
		buf[97] = 0x00;
		buf[98] = 0x00;
		buf[99] = 0xff;
		buf[100] = 0xff;
		buf[101] = 0xff;
		buf[102] = 0xff;
		buf[103] = 0xff;
		buf[104] = 0xfa;
		buf[105] = 0x00;
		buf[106] = 0xff;
		buf[107] = 0xff;
		buf[108] = 0x00;
		buf[109] = 0x00;
		buf[110] = 0xff;
		buf[111] = 0x00;
		buf[112] = 0x00;
		buf[113] = 0xff;
		buf[114] = 0x00;
		buf[115] = 0x00;
		buf[116] = 0x42;
		
		/* LOD */
		buf[130] = 0x01;
	}
	
	/// Set the mouse to use the glorious animation
	pub fn set_animation_glorious(&mut self, speed: Speed, direction: UpDownDirection) {
		self.data[53] = 0x01; // Animation (1 = glorious)
		self.data[54] = 4*16 + (speed as u8); // Glorious speed
		self.data[55] = direction as u8;
	}
	
	/// Set the mouse to use the single color animation
	pub fn set_animation_single(&mut self, brightness: Brightness, color: Color) {
		self.data[53] = 0x02; // Animation (2 = Single color)
		self.data[56] = brightness as u8;
		
		// Yes, RBG
		self.data[57] = color.r as u8;
		self.data[58] = color.b as u8;
		self.data[59] = color.g as u8;
	}
	
	/// Set tail animation
	pub fn set_animation_tail(&mut self, brightness: Brightness, speed: Speed) {
		self.data[53] = 0x04; // Animation (4 = Tail)
		self.data[83] = brightness as u8 + speed as u8;
	}
	
	/// Set off animation
	pub fn set_animation_off(&mut self) {
		self.data[53] = 0x0; // Animation (0 = off)
	}
	
	/// Set single color breathing animation
	pub fn set_animation_single_color_breathing(&mut self, speed: Speed, color: Color) {
		self.data[53] = 0x0a; // Animation (0x0a = single color breathing)
		
		self.data[125] = speed as u8;
		self.data[126] = color.r as u8;
		self.data[127] = color.b as u8;
		self.data[128] = color.g as u8;
	}
	
	/// Set RGB breathing animation
	pub fn set_animation_breathing_rgb(&mut self, speed: Speed) {
		self.data[53] = 0x05; // Animation (5 = RGB breathing)
		
		self.data[84] = 0x40 + speed as u8;
		// I wonder if the bytes after this one control parameters for this animation that the gui doesn't expose
	}
	
	/// Set wave animation
	pub fn set_animation_wave(&mut self, brightness: Brightness, speed: Speed) {
		self.data[53] = 0x09; // Animation (9 = wave)
		
		self.data[124] = brightness as u8 + speed as u8;
	}
	
	/// Set animation 6
	pub fn set_animation_6(&mut self, colors: &[Color]) -> Result<(), Box<dyn std::error::Error>> {
		// TODO: This shouldn't error, should be handled cleanly
		if colors.len() > 6 {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Too many colors")));
		}
		
		self.data[53] = 0x06; // Animation (6 = ???(Individual segments))
		
		for (i, color) in colors.iter().enumerate() {
			self.data[86 + i*3 + 0] = color.r;
			self.data[86 + i*3 + 1] = color.b;
			self.data[86 + i*3 + 2] = color.g;
		}
		
		Ok(())
	}
	
	/// Set breathing animation
	pub fn set_animation_breathing(&mut self, speed: Speed, colors: &[Color]) -> Result<(), Box<dyn std::error::Error>> {
		// TODO: >7 colors should be handled cleanly
		if colors.len() < 2 || colors.len() > 7 {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid number of colors (Need 2-7)")));
		}
		
		self.data[53] = 0x03; // Animation (3 = breathing)
		
		self.data[60] = 0x40 + speed as u8;
		self.data[61] = colors.len() as u8;
		
		for (i, color) in colors.iter().enumerate() {
			self.data[62 + i*3 + 0] = color.r;
			self.data[62 + i*3 + 1] = color.b;
			self.data[62 + i*3 + 2] = color.g;
		}
		
		Ok(())
	}

	/// Set rave animation
	pub fn set_animation_rave(&mut self, speed: Speed, brightness: Brightness, color1: Color, color2: Color) {
		self.data[53] = 0x07; // Animation (7 = rave)
		
		self.data[116] = brightness as u8 + speed as u8;
		
		self.data[117] = color1.r as u8;
		self.data[118] = color1.b as u8;
		self.data[119] = color1.g as u8;
		self.data[120] = color2.r as u8;
		self.data[121] = color2.b as u8;
		self.data[122] = color2.g as u8;
	}
	
	/// Set a dpi profile's values
	pub fn set_dpi_profile(&mut self, index: usize, dpi: i32, color: Color) -> Result<(), Box<dyn std::error::Error>> {
		/* TODO: X != Y */
		if index >= self.profiles_max as usize {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Dpi profile out of range")));
		}
		
		if dpi < 400 || dpi > 12000 {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Dpi number out of range (400-12000)")));
		}
		
		/* Set dpi */
		self.data[13 + index] = (dpi/100 -1) as u8; // Dpi profile 1 X = Y dpi
		
		/* Set RGB of the dpi */
		self.data[29 + (index*3) + 0] = color.r;
		self.data[29 + (index*3) + 1] = color.g;
		self.data[29 + (index*3) + 2] = color.b;
		
		Ok(())
	}
	
	/// Set polling rate
	pub fn set_polling_rate(&mut self, pollingrate: PollingRate) {
		self.data[10] &= 0xF0;
		self.data[10] |= pollingrate as u8;
	}
	
	/// Set LOD
	pub fn set_lod(&mut self, lod: LOD) {
		self.data[130] = lod as u8;
	}
	
	/// Set enabled dpi profiles
	pub fn set_enabled_dpi_profiles(&mut self, profiles: &[u8], activeindex: u8) -> Result<(), Box<dyn std::error::Error>> {
		if profiles.is_empty() || profiles.len() > (self.profiles_max) as usize {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Invalid number of dpi profiles")));
		}
		
		if activeindex > (profiles.len() - 1) as u8 {
			return Err(Box::new(Error::new(ErrorKind::NotFound, "Dpi profile index out of range")));
		}
		
		let activeindex = activeindex + 1;
		
		// First half: index of currently enabled profile, Second half: number of enabled profiles
		self.data[11] = activeindex * 0x10 + profiles.len() as u8;
		
		// (Inverse?)Bitfield of enabled dpi profiles
		self.data[12] = 0xFF;
		for profile in profiles {
			if profile < &self.profiles_max {// TODO: Error
				self.data[12] ^= 1 << *profile;
			}
		}
		
		Ok(())
	}
	
	/// Write values to the device
	pub fn update_device(&mut self) -> Result<(), Box<dyn std::error::Error>> {
		self.write_control(0x0304, &self.data.clone())
	}
	
	/// Set Debounce
	pub fn update_debounce(&mut self, debounce: u8) -> Result<(), Box<dyn std::error::Error>> {
		// TODO: Update on update_device instead of immediately on call
		let mut buf: Vec<u8> = vec!(0; 6);
		buf[0] = 0x05;
		buf[1] = 0x1a;
		buf[2] = debounce/2; // Debounce time (2 = 4ms, 8 = 16ms, 6 = 12ms, etc)
		self.write_control(0x0305, &buf)
	}
	
	/// Print data that would be sent
	pub fn print_data(&self) {
		/* Print data */
		let chunks = self.data.chunks(16);
		for chunk in chunks {
			println!("{:x?}", chunk);
		}
	}
}
