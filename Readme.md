# Glorious Model O- mouse configuration utility for linux(And possibly other systems that both rust and libusb support, I guess?)

## USE AT YOUR OWN RISK
Seems to work on my mouse, and a few others.
Only tested on **O-**, not O or D or other mice.
Only tested with firmware version **1.0.9**. 
Model D apparently has a different usb id, definitely won't work out of the box.

## Running
It will replace **ALL** of your mouse's settings, not only the things you explicitly specify.

`cargo build; sudo ./target/debug/magnificent --help`
```
Magnificent 0.1.0
malice <derpmalicious@gmail.com>
Glorious Model O- configuration utility

USAGE:
    magnificent [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --animation <FILE>           Sets the animation [default: glorious]  [possible values: glorious, single, tail,
                                     off, breathing_rgb, breathing_single, rave, wave, breathing, 6]
    -b, --brightness <BRIGHTNESS>    Sets the animation brightness [default: 100]  [possible values: 25, 50, 75, 100]
    -c, --colors <COLORS>            Sets the colors used in animations (Space separated list) [default: ff0000]
    -e, --debounce <DEBOUNCE>        Sets the debounce time [default: 4]
    -j, --direction <DIRECTION>      Sets the animation direction [default: down]  [possible values: up, down]
    -l, --lod <LOD>                  Sets the LOD [default: 2]  [possible values: 2, 3]
    -p, --poll <POLLING_RATE>        Sets the polling rate [default: 1000]  [possible values: 125, 250, 500, 1000]
    -d, --profiles <PROFILES>        Sets the dpi profiles (Space separated list) [default: 1600:ff0000]
    -s, --speed <SPEED>              Sets the animation speed [default: 2]  [possible values: 1, 2, 3]
```

## Examples:
`magnificent --animation 6 --debounce 2 --profiles 1600:ff0000 --colors ff0000 fff700 ffff00 00ff00 0000ff ff00ff`
`magnificent --animation glorious --direction up --speed 3 --debounce 2 --profiles 1600:ff0000 3200:00ff00 6400:ffffff`
`magnificent --animation single --brightness 100 --debounce 2 --profiles 1600:ff0000 --colors ff0000`

If you trigger an error related to invalid arguments being passed, the mouse MIGHT stop working, as the kernel driver is detached.
Try running it again with correct arguments. Replugging the mouse should also work.
In some cases, the command might fail, and rerunning the utility might fix it
